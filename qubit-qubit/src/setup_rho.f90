subroutine setup_rho( )
  !
  use commvars,   only : DP, eps, rho, lb0, IA, IAp, stdout, steering
  !
  implicit none
  !
  real(DP) :: theta(0:3,0:3), b(1:3), rndm(1)
  complex(DP) :: rhob(0:1,0:1), rhobm1(0:1,0:1), rhobm12(0:1,0:1), &
                 tmp(0:3,0:3), uv(0:3,0:3), uvdag(0:3,0:3), s0(0:1,0:1)
  integer :: i, j, ios
  !
  complex(DP), parameter :: p1 = cmplx(1.d0,0.d0),  m1 = cmplx(-1.d0,0.d0), &
                            pi = cmplx(0.d0,1.d0),  mi = cmplx(0.d0,-1.d0), &
                            zero = cmplx(0.d0,0.d0)
  !
  !call random_number( rndm )
  !state_id = rndm(1)
  !
  s0(0:1,0:1) = zero; s0(0,0) = p1; s0(1,1) = p1
  theta(0:3,0:3) = rho(0:3,0:3)
  write(stdout,'(/,A)')"==========================================="
  write(stdout,*)"The input THETA-matrix"
  do i = 0, 3
    write(stdout,'(4(f12.8))') (theta(i,j), j=0,3 )
  enddo
  if ( steering.eq.'AB') then
    write(stdout,'(/,5X,"Steering from Alice to Bob")')
  else
    theta(0:3,0:3) = transpose( theta(0:3,0:3))
    write(stdout,'(/,5X,"Steering from Bob to Alice")')
  endif
  !
  b(1:3) = rho(0,1:3)
  rho(0:3,0:3) = 0.5d0*transpose( rho(0:3,0:3))
  !
  ! Transform to state with b=(0,0,0)
  !
  if ( lb0 ) then
    !
    call Theta2rho( theta(0:3,0:3), tmp(0:3,0:3) )
    !write(*,*)
    !do i = 0, 3
    !  write(*,'(4(f12.8))') (dble(tmp(i,j)), j=0,3 )
    !enddo
    !do i = 0, 3
    !  write(*,'(4(f12.8))') (aimag(tmp(i,j)), j=0,3 )
    !enddo
    !
    call PTraceA( tmp(0:3,0:3), rhob(0:1,0:1) )
    call inv_rho2x2( rhob(0:1,0:1), rhobm1(0:1,0:1) )
    call sqrt_rho2x2( rhobm1(0:1,0:1), rhobm12(0:1,0:1) )
    !
    call KroneckerProduct( s0(0:1,0:1), rhobm12(0:1,0:1), uv(0:3,0:3) )
    !write(*,*)
    !do i = 0, 3
    !  write(*,'(4(f12.8))') (dble(uv(i,j)), j=0,3 )
    !enddo
    !do i = 0, 3
    !  write(*,'(4(f12.8))') (aimag(uv(i,j)), j=0,3 )
    !enddo
    !uvdag(0:3,0:3) = conjg(uv(0:3,0:3))
    !tmp(0:3,0:3) = matmul( tmp(0:3,0:3), conjg( transpose(uv(0:3,0:3))) )
    tmp(0:3,0:3) = matmul( tmp(0:3,0:3), uv(0:3,0:3) )
    tmp(0:3,0:3) = matmul( uv(0:3,0:3), tmp(0:3,0:3) )
    tmp(0:3,0:3) = tmp(0:3,0:3) / (tmp(0,0)+tmp(1,1)+tmp(2,2)+tmp(3,3))
    !
    call rho2Theta( tmp(0:3,0:3), rho(0:3,0:3) )
    !
    b(1:3) = rho(0,1:3)
    write(stdout,'(/,A)')"==========================================="
    write(stdout,*)"State in ""canonical form"" (b=0)"
    do i = 0, 3
      write(stdout,'(4(f12.8))') (rho(i,j), j=0,3 )
    enddo
    rho(0:3,0:3) = 0.5d0*transpose( rho(0:3,0:3))
    !
  endif
  !
  ! image of IA via the EPR map
  !
  IAp(0:3) = matmul( rho(0:3,0:3), IA(0:3) ) 
  !write(stdout,'(/,"Image of IA via EPR map:",/,4(F15.12))') IAp(0:3) 
  !
end subroutine setup_rho
!
!
subroutine Theta2rho( Theta, rho )
  !
  use commvars,  only : DP
  !
  implicit none
  !
  real(DP), intent(in) :: Theta(0:3,0:3)
  complex(DP), intent(out) :: rho(0:3,0:3)
  !
  complex(DP), dimension(0:3,0:3) :: s00, s01, s02, s03, s10, s11, s12, s13, &
                                     s20, s21, s22, s23, s30, s31, s32, s33
  complex(DP), parameter :: p1 = cmplx(1.d0,0.d0),  m1 = cmplx(-1.d0,0.d0), &
                            pi = cmplx(0.d0,1.d0),  mi = cmplx(0.d0,-1.d0), &
                            zero = cmplx(0.d0,0.d0)
  !
  s00(0:3,0:3) = zero; s00(0,0) = p1; s00(1,1) = p1; s00(2,2) = p1; s00(3,3) = p1;
  s01(0:3,0:3) = zero; s01(0,1) = p1; s01(1,0) = p1; s01(2,3) = p1; s01(3,2) = p1;
  s02(0:3,0:3) = zero; s02(0,1) = mi; s02(1,0) = pi; s02(2,3) = mi; s02(3,2) = pi;
  s03(0:3,0:3) = zero; s03(0,0) = p1; s03(1,1) = m1; s03(2,2) = p1; s03(3,3) = m1;
  !
  s10(0:3,0:3) = zero; s10(0,2) = p1; s10(1,3) = p1; s10(2,0) = p1; s10(3,1) = p1;
  s11(0:3,0:3) = zero; s11(0,3) = p1; s11(1,2) = p1; s11(2,1) = p1; s11(3,0) = p1;
  s12(0:3,0:3) = zero; s12(0,3) = mi; s12(1,2) = pi; s12(2,1) = mi; s12(3,0) = pi;
  s13(0:3,0:3) = zero; s13(0,2) = p1; s13(1,3) = m1; s13(2,0) = p1; s13(3,1) = m1;
  !
  s20(0:3,0:3) = zero; s20(0,2) = mi; s20(1,3) = mi; s20(2,0) = pi; s20(3,1) = pi;
  s21(0:3,0:3) = zero; s21(0,3) = mi; s21(1,2) = mi; s21(2,1) = pi; s21(3,0) = pi;
  s22(0:3,0:3) = zero; s22(0,3) = m1; s22(1,2) = p1; s22(2,1) = p1; s22(3,0) = m1;
  s23(0:3,0:3) = zero; s23(0,2) = mi; s23(1,3) = pi; s23(2,0) = pi; s23(3,1) = mi;
  !
  s30(0:3,0:3) = zero; s30(0,0) = p1; s30(1,1) = p1; s30(2,2) = m1; s30(3,3) = m1;
  s31(0:3,0:3) = zero; s31(0,1) = p1; s31(1,0) = p1; s31(2,3) = m1; s31(3,2) = m1;
  s32(0:3,0:3) = zero; s32(0,1) = mi; s32(1,0) = pi; s32(2,3) = pi; s32(3,2) = mi;
  s33(0:3,0:3) = zero; s33(0,0) = p1; s33(1,1) = m1; s33(2,2) = m1; s33(3,3) = p1;
  !
  rho(0:3,0:3) = zero
  rho(0:3,0:3) = rho(0:3,0:3) + Theta(0,0)*s00(0:3,0:3)
  rho(0:3,0:3) = rho(0:3,0:3) + Theta(0,1)*s01(0:3,0:3)
  rho(0:3,0:3) = rho(0:3,0:3) + Theta(0,2)*s02(0:3,0:3)
  rho(0:3,0:3) = rho(0:3,0:3) + Theta(0,3)*s03(0:3,0:3)
  !
  rho(0:3,0:3) = rho(0:3,0:3) + Theta(1,0)*s10(0:3,0:3)
  rho(0:3,0:3) = rho(0:3,0:3) + Theta(1,1)*s11(0:3,0:3)
  rho(0:3,0:3) = rho(0:3,0:3) + Theta(1,2)*s12(0:3,0:3)
  rho(0:3,0:3) = rho(0:3,0:3) + Theta(1,3)*s13(0:3,0:3)
  !
  rho(0:3,0:3) = rho(0:3,0:3) + Theta(2,0)*s20(0:3,0:3)
  rho(0:3,0:3) = rho(0:3,0:3) + Theta(2,1)*s21(0:3,0:3)
  rho(0:3,0:3) = rho(0:3,0:3) + Theta(2,2)*s22(0:3,0:3)
  rho(0:3,0:3) = rho(0:3,0:3) + Theta(2,3)*s23(0:3,0:3)
  !
  rho(0:3,0:3) = rho(0:3,0:3) + Theta(3,0)*s30(0:3,0:3)
  rho(0:3,0:3) = rho(0:3,0:3) + Theta(3,1)*s31(0:3,0:3)
  rho(0:3,0:3) = rho(0:3,0:3) + Theta(3,2)*s32(0:3,0:3)
  rho(0:3,0:3) = rho(0:3,0:3) + Theta(3,3)*s33(0:3,0:3)
  !
  rho(0:3,0:3) = 0.25d0*rho(0:3,0:3)
  !
end subroutine Theta2rho
!
!
subroutine rho2Theta( rho, Theta )
  !
  use commvars,  only : DP
  !
  implicit none
  !
  complex(DP), intent(in) :: rho(0:3,0:3)
  real(DP), intent(out) :: Theta(0:3,0:3)
  !
  complex(DP), dimension(0:3,0:3) :: s00, s01, s02, s03, s10, s11, s12, s13, &
                                     s20, s21, s22, s23, s30, s31, s32, s33, ss
  complex(DP), parameter :: p1 = cmplx(1.d0,0.d0),  m1 = cmplx(-1.d0,0.d0), &
                            pi = cmplx(0.d0,1.d0),  mi = cmplx(0.d0,-1.d0), &
                            zero = cmplx(0.d0,0.d0)
  !
  s00(0:3,0:3) = zero; s00(0,0) = p1; s00(1,1) = p1; s00(2,2) = p1; s00(3,3) = p1;
  s01(0:3,0:3) = zero; s01(0,1) = p1; s01(1,0) = p1; s01(2,3) = p1; s01(3,2) = p1;
  s02(0:3,0:3) = zero; s02(0,1) = mi; s02(1,0) = pi; s02(2,3) = mi; s02(3,2) = pi;
  s03(0:3,0:3) = zero; s03(0,0) = p1; s03(1,1) = m1; s03(2,2) = p1; s03(3,3) = m1;
  !
  s10(0:3,0:3) = zero; s10(0,2) = p1; s10(1,3) = p1; s10(2,0) = p1; s10(3,1) = p1;
  s11(0:3,0:3) = zero; s11(0,3) = p1; s11(1,2) = p1; s11(2,1) = p1; s11(3,0) = p1;
  s12(0:3,0:3) = zero; s12(0,3) = mi; s12(1,2) = pi; s12(2,1) = mi; s12(3,0) = pi;
  s13(0:3,0:3) = zero; s13(0,2) = p1; s13(1,3) = m1; s13(2,0) = p1; s13(3,1) = m1;
  !
  s20(0:3,0:3) = zero; s20(0,2) = mi; s20(1,3) = mi; s20(2,0) = pi; s20(3,1) = pi;
  s21(0:3,0:3) = zero; s21(0,3) = mi; s21(1,2) = mi; s21(2,1) = pi; s21(3,0) = pi;
  s22(0:3,0:3) = zero; s22(0,3) = m1; s22(1,2) = p1; s22(2,1) = p1; s22(3,0) = m1;
  s23(0:3,0:3) = zero; s23(0,2) = mi; s23(1,3) = pi; s23(2,0) = pi; s23(3,1) = mi;
  !
  s30(0:3,0:3) = zero; s30(0,0) = p1; s30(1,1) = p1; s30(2,2) = m1; s30(3,3) = m1;
  s31(0:3,0:3) = zero; s31(0,1) = p1; s31(1,0) = p1; s31(2,3) = m1; s31(3,2) = m1;
  s32(0:3,0:3) = zero; s32(0,1) = mi; s32(1,0) = pi; s32(2,3) = pi; s32(3,2) = mi;
  s33(0:3,0:3) = zero; s33(0,0) = p1; s33(1,1) = m1; s33(2,2) = m1; s33(3,3) = p1;
  !
  ss(0:3,0:3) = matmul( rho(0:3,0:3), s00(0:3,0:3) )
  theta(0,0) = 1.d0*( ss(0,0)+ss(1,1)+ss(2,2)+ss(3,3) )
  !
  ss(0:3,0:3) = matmul( rho(0:3,0:3), s01(0:3,0:3) )
  theta(0,1) = 1.d0*( ss(0,0)+ss(1,1)+ss(2,2)+ss(3,3) )
  !
  ss(0:3,0:3) = matmul( rho(0:3,0:3), s02(0:3,0:3) )
  theta(0,2) = 1.d0*( ss(0,0)+ss(1,1)+ss(2,2)+ss(3,3) )
  !
  ss(0:3,0:3) = matmul( rho(0:3,0:3), s03(0:3,0:3) )
  theta(0,3) = 1.d0*( ss(0,0)+ss(1,1)+ss(2,2)+ss(3,3) )
  !
  ss(0:3,0:3) = matmul( rho(0:3,0:3), s10(0:3,0:3) )
  theta(1,0) = 1.d0*( ss(0,0)+ss(1,1)+ss(2,2)+ss(3,3) )
  !
  ss(0:3,0:3) = matmul( rho(0:3,0:3), s11(0:3,0:3) )
  theta(1,1) = 1.d0*( ss(0,0)+ss(1,1)+ss(2,2)+ss(3,3) )
  !
  ss(0:3,0:3) = matmul( rho(0:3,0:3), s12(0:3,0:3) )
  theta(1,2) = 1.d0*( ss(0,0)+ss(1,1)+ss(2,2)+ss(3,3) )
  !
  ss(0:3,0:3) = matmul( rho(0:3,0:3), s13(0:3,0:3) )
  theta(1,3) = 1.d0*( ss(0,0)+ss(1,1)+ss(2,2)+ss(3,3) )
  !
  ss(0:3,0:3) = matmul( rho(0:3,0:3), s20(0:3,0:3) )
  theta(2,0) = 1.d0*( ss(0,0)+ss(1,1)+ss(2,2)+ss(3,3) )
  !
  ss(0:3,0:3) = matmul( rho(0:3,0:3), s21(0:3,0:3) )
  theta(2,1) = 1.d0*( ss(0,0)+ss(1,1)+ss(2,2)+ss(3,3) )
  !
  ss(0:3,0:3) = matmul( rho(0:3,0:3), s22(0:3,0:3) )
  theta(2,2) = 1.d0*( ss(0,0)+ss(1,1)+ss(2,2)+ss(3,3) )
  !
  ss(0:3,0:3) = matmul( rho(0:3,0:3), s23(0:3,0:3) )
  theta(2,3) = 1.d0*( ss(0,0)+ss(1,1)+ss(2,2)+ss(3,3) )
  !
  ss(0:3,0:3) = matmul( rho(0:3,0:3), s30(0:3,0:3) )
  theta(3,0) = 1.d0*( ss(0,0)+ss(1,1)+ss(2,2)+ss(3,3) )
  !
  ss(0:3,0:3) = matmul( rho(0:3,0:3), s31(0:3,0:3) )
  theta(3,1) = 1.d0*( ss(0,0)+ss(1,1)+ss(2,2)+ss(3,3) )
  !
  ss(0:3,0:3) = matmul( rho(0:3,0:3), s32(0:3,0:3) )
  theta(3,2) = 1.d0*( ss(0,0)+ss(1,1)+ss(2,2)+ss(3,3) )
  !
  ss(0:3,0:3) = matmul( rho(0:3,0:3), s33(0:3,0:3) )
  theta(3,3) = 1.d0*( ss(0,0)+ss(1,1)+ss(2,2)+ss(3,3) )
  !
end subroutine rho2Theta
!
!
subroutine KroneckerProduct( a, b, ab )
  !
  use commvars,  only : DP
  !
  implicit none
  !
  complex(DP), intent(in) :: a(0:1,0:1), b(0:1,0:1)
  complex(DP), intent(out) :: ab(0:3,0:3)
  !
  ab(0:1,0:1) = a(0,0)*b(0:1,0:1)
  ab(0:1,2:3) = a(0,1)*b(0:1,0:1)
  ab(2:3,0:1) = a(1,0)*b(0:1,0:1)
  ab(2:3,2:3) = a(1,1)*b(0:1,0:1)
  !
end subroutine KroneckerProduct
!
!
subroutine PTraceA( rho, rhob )
  !
  use commvars,  only : DP
  !
  implicit none
  !
  complex(DP), intent(in) :: rho(0:3,0:3)
  complex(DP), intent(out) :: rhob(0:1,0:1)
  !
  rhob(0,0) = rho(0,0) + rho(2,2)
  rhob(1,1) = rho(1,1) + rho(3,3)
  rhob(0,1) = rho(0,1) + rho(2,3)
  rhob(1,0) = rho(1,0) + rho(3,2)
  !
end subroutine PTraceA
!
!
subroutine PTraceB( rho, rhoa )
  !
  use commvars,  only : DP
  !
  implicit none
  !
  complex(DP), intent(in) :: rho(0:3,0:3)
  complex(DP), intent(out) :: rhoa(0:1,0:1)
  !
  rhoa(0,0) = rho(0,0) + rho(1,1)
  rhoa(1,1) = rho(2,2) + rho(3,3)
  rhoa(0,1) = rho(0,2) + rho(1,3)
  rhoa(1,0) = rho(2,0) + rho(3,1)
  !
end subroutine PTraceB
!
!
subroutine inv_rho2x2( rho, rhom1 )
  !
  use commvars,  only : DP, eps
  !
  implicit none
  !
  complex(DP), intent(in) :: rho(0:1,0:1)
  complex(DP), intent(out) :: rhom1(0:1,0:1)
  !
  complex(DP) :: det
  !
  det = rho(0,0)*rho(1,1) - rho(0,1)*rho(1,0)
  if ( abs(det).lt.eps(8) ) then
    write(*,*)"inv_rho2x2: input rho is singular! STOP!!!"
    stop
  else
    rhom1(0,0) =  rho(1,1) / det
    rhom1(1,1) =  rho(0,0) / det
    rhom1(0,1) = -rho(0,1) / det
    rhom1(1,0) = -rho(1,0) / det
  endif 
  !
end subroutine inv_rho2x2
!
!
subroutine sqrt_rho2x2( rho, rho12 )
  !
  use commvars,  only : DP, eps
  !
  implicit none
  !
  complex(DP), intent(in) :: rho(0:1,0:1)
  complex(DP), intent(out) :: rho12(0:1,0:1)
  !
  complex(DP), parameter :: p1 = cmplx(1.d0,0.d0),  m1 = cmplx(-1.d0,0.d0), &
                            pi = cmplx(0.d0,1.d0),  mi = cmplx(0.d0,-1.d0), &
                            zero = cmplx(0.d0,0.d0)
  !
  complex(DP) :: s0(0:1,0:1)
  complex(DP) :: tau, s, delta, t
  !
  s0(0:1,0:1) = zero; s0(0,0) = p1; s0(1,1) = p1
  !
  tau = rho(0,0) + rho(1,1)
  delta = rho(0,0)*rho(1,1) - rho(0,1)*rho(1,0)
  s = sqrt(delta); t = sqrt(tau+cmplx(2.d0,0.d0)*s)
  !
  rho12 = p1/t * ( rho(0:1,0:1) + s*s0 )
  !
end subroutine sqrt_rho2x2
!
!
