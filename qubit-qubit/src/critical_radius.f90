program critical_radius
  !
  use commvars, only : DP, eps, calculation, ui, nui, r_in, lbloch, &
                       lasym, r_u, rlo, rup, r_u_lp, dmu, ind3, c3nui, stdout, &
                       rho, nstates, thetas, state_id, ui_inner, ui_outer
  !
  implicit none
  !
  logical :: lp_converged, first, lr_u, lr_cut, lconstr
  integer :: iter, k, l, i, j, id
  real(DP) :: fac_ddmu, fac
  character(len=8) :: calc
  !
  character(len=1), external :: capital 
  !
  call readin( )
  !
  call gen_polytopes( )
  !
  k = len_trim(calculation); calc = ' '
  do l = 1, k
    calc(l:l) = capital ( calculation(l:l) )
  enddo
  !
  ! Loop over states
  !
  do id = 1, nstates
    !
    if ( trim(state_id(id)).eq.'invalid' ) then
      !
      write(stdout,'(/,"-----------------------------------------------------",/)')
      write(stdout,'(/,"State with state_id = ",A,": WRONG THETA matrix")') trim(state_id(id))
      cycle
      !
    else
      !
      write(stdout,'(/,"-----------------------------------------------------",/)')
      write(stdout,'(/,"State with state_id = ",A)') trim(state_id(id))
      !
      do j = 0, 3
        rho(0:3,j) = thetas(1+4*j:4+4*j,id)
      enddo
      call setup_rho( )
      !
      if ( (calc.eq.'R_LOWER') .or. (calc.eq.'R_BOUNDS') ) then 
        !
        write(stdout,'(/,1x,"Compute a lower bound for the critical radius")')
        lbloch = .true.; ui(:,:) = ui_inner(:,:)
        !
        call compute_all_constraints( )
        call cplex_lp_optimization( )
        write(stdout,'(3x,"Check LP solution ... ")',advance='no')
        call read_opt_lhs( )
        call compute_prds( )
        if ( abs(r_u-r_u_lp).gt.eps(5) ) then
          write(stdout,'(2x,"WRONG (diff:",F17.12,")")')abs(r_u-r_u_lp)
          write(stdout,'(5x,2(A,F17.12,3x))')"r_u_lp =", r_u_lp, "r_u =", r_u
          stop
        else
          rlo = r_u_lp; write(stdout,'(2x,"GOOD (diff:",F17.12,")")')abs(r_u-r_u_lp)
          write(stdout,'(5x,2(A,F17.12,3x))')"r_u_lp =", r_u_lp, "r_u =", r_u
        endif
        !
      endif
      !
      !
      if ( (calc.eq.'R_UPPER') .or. (calc.eq.'R_BOUNDS') ) then
        !
        write(stdout,'(/,1x,"Compute an upper bound for the critical radius")')
        lbloch = .false.; ui(:,:) = ui_outer(:,:)
        !
        call compute_all_constraints( )
        call cplex_lp_optimization( )
        write(stdout,'(3x,"Check LP solution ... ")',advance='no')
        call read_opt_lhs( )
        call compute_prds( )
        if ( abs(r_u-r_u_lp).gt.eps(5) ) then
          write(stdout,'(2x,"WRONG (diff:",F17.12,")")')abs(r_u-r_u_lp)
          write(stdout,'(5x,2(A,F17.12,3x))')"r_u_lp =", r_u_lp, "r_u =", r_u
          stop
        else
          rup=r_u_lp; write(stdout,'(2x,"GOOD (diff:",F17.12,")")')abs(r_u-r_u_lp)
          write(stdout,'(5x,2(A,F17.12,3x))')"r_u_lp =", r_u_lp, "r_u =", r_u
        endif
        !
        !write(stdout,'(/,1x,"Done",/)')
        !
      endif
      !
      !
      if ( calc.eq.'R_U' ) then
        !
        write(stdout,'(1x,"Compute the principle radius",/)')
        call compute_prds( )
        write(stdout,'(3x,A,I5,f18.12)') "PRINCIPAL RADIUS r_u: ", nui, r_u
        !   
      endif
      !
      if ( calc.eq.'R_BOUNDS' ) then
        write(stdout,'(/,"! state_id = ",A,A,F12.8,A,F12.8 )') &
              trim(state_id(id)), "  R_lower = ", rlo, "  R_upper = ", rup
      elseif (calc.eq.'R_LOWER') then
        write(stdout,'(/,"! state_id = ",A,A,F12.8)') &
              trim(state_id(id)), "  R_lower = ", rlo
      elseif (calc.eq.'R_UPPER') then
        write(stdout,'(/,"! state_id = ",A,A,F12.8 )') &
              trim(state_id(id)), "  R_upper = ", rup
      else
        write(stdout,*) "no calculation is specified "
        stop
      endif
      !
    endif
    !
  enddo
  !
  !call write_prds_input( )
  !call write_opt_u( )
  !call write_povm_input( )
  deallocate( ind3 )
  !
  !
  write(stdout,'(/,"CALCULATION DONE!",/)')
  !
end program critical_radius
!
!
subroutine cplex_lp_optimization( )
  !
  use commvars,  only: DP, tempdir, cplexdir, cplexmem, stdout
  !
  implicit none
  !
  character(len=132) :: cmd
  integer :: cstat, estat
  character(len=100) :: cmsg
  !
  write(stdout,'(3x,"Optimizing the distribution mu by CPLEX ... ")', advance='no')
  !
  cmd = "rm -f "//trim(tempdir)//"/weights.sol"
  call execute_command_line ( trim(cmd) , EXITSTAT=estat, &
                             CMDSTAT=cstat, CMDMSG=cmsg)
  !
  open( 1, file="cplex.in", status='unknown', form='formatted', action='write' )
  rewind(1)
  write(1,*) "set emphasis memory yes"
  write(1,*) "set workdir "//trim(tempdir)
  write(1,*) "set workmem ", cplexmem
  write(1,*) "read "//trim(tempdir)//"/optmzui.lp"
  write(1,*) "optimize"
  write(1,*) "barrier convergetol 1e-10"
  write(1,*) "write "//trim(tempdir)//"/weights.sol"
  close(1,status='keep')
  !
  cmd = trim(cplexdir)//"/cplex < cplex.in > /dev/null"
  call execute_command_line ( trim(cmd) , EXITSTAT=estat, &
                             CMDSTAT=cstat, CMDMSG=cmsg)
  if (cstat > 0) then
    write(stdout,*) "CPLEX: command execution failed with error ", TRIM(CMSG)
    stop
  else if (cstat < 0) then
    write(stdout,*)"CPLEX: command execution not supported"
  !ELSE
  !  PRINT *, "Command completed with status ", ESTAT
  end if
  !
  open( 1, file="sol2txt.py", status='unknown', form='formatted', action='write' )
  rewind(1)
  write(1,'(A)')'import sys'
  write(1,'(A)')'import os'
  write(1,'(A)')
  write(1,'(A)')'def main():'
  write(1,'(A)')'    filepath = sys.argv[1]'
  write(1,'(A)')'    if not os.path.isfile(filepath):'
  write(1,'(A)')'        print("File path {} does not exist. Exiting...".format(filepath))'
  write(1,'(A)')'        sys.exit()'
  write(1,'(A)')
  write(1,'(A)')'    bag_of_words = {}'
  write(1,'(A)')'    with open(filepath) as fp:'
  write(1,'(A)')'       for line in fp:'
  write(1,'(A)')'           if ( line.strip().split(" ")[0]=="<variable" ):'
  write(1,'(A)')'               print(line.strip().split(" ")[4].split("\"")[1]);'
  write(1,'(A)')
  write(1,'(A)')'if __name__ == "__main__":'
  write(1,'(A)')'    main()'
  close(1,status='keep')

  cmd="python sol2txt.py "//trim(tempdir)//"/weights.sol > weights.txt"
  call execute_command_line ( trim(cmd) , EXITSTAT=estat, &
                             CMDSTAT=cstat, CMDMSG=cmsg)
  !
  if (cstat > 0) then
    write(stdout,*) "python: command execution failed with error ", TRIM(CMSG)
    stop
  else if (cstat < 0) then
    write(stdout,*)"python: command execution not supported"
  else
    open( 1, file="sol2txt.py", status='unknown', form='formatted' )
    close(1, status='delete')
  end if
  !
  write(stdout,'(" done!")')
  !
end subroutine cplex_lp_optimization
!
!
subroutine write_povm_input( )
  !
  use commvars, only: DP, ndim, ui, dmu, nui, r_u, rho, winit, state_id 
  !
  implicit none
  !
  integer :: i, j
  real(DP) :: theta(0:3,0:3)
  real(DP) :: rndm(1)
  !
  !call random_number( rndm )
  !state_id = rndm(1)
  !
  open( 1, file="povm.in.txt", status='unknown', form='formatted', action='write' )
  write(1,'(F17.12,A)',advance='no') r_u, achar(9)
  write(1,'(F17.12,A)',advance='no') state_id, achar(9)
  !
  theta(0:3,0:3) = 2.d0*transpose( rho(0:3,0:3) )
  do i = 0, ndim
    do j = 0, ndim
      write(1,'(F17.12,A)',advance='no') theta(j,i), achar(9)
    enddo
  enddo
  write(1,'(I5,A)',advance='no') nui, achar(9)
  do i = 1, nui
    write(1,'(F17.12,A)',advance='no') dmu(i), achar(9)
    write(1,'(F17.12,A)',advance='no') ui(1,i), achar(9)
    write(1,'(F17.12,A)',advance='no') ui(2,i), achar(9)
    write(1,'(F17.12,A)',advance='no') ui(3,i), achar(9)
  enddo
  close(1)
  !
end subroutine write_povm_input
!
!-----------------------------------------------------------------------
FUNCTION capital( in_char )
  !-----------------------------------------------------------------------
  !
  ! ... converts character to capital if lowercase
  ! ... copy character to output in all other cases
  !
  IMPLICIT NONE
  !
  CHARACTER(LEN=1), INTENT(IN) :: in_char
  CHARACTER(LEN=1)             :: capital
  CHARACTER(LEN=26), PARAMETER :: lower = 'abcdefghijklmnopqrstuvwxyz', &
                                  upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  INTEGER                      :: i
  !
  !
  DO i=1, 26
     !
     IF ( in_char == lower(i:i) ) THEN
        !
        capital = upper(i:i)
        !
        RETURN
        !
     END IF
     !
  END DO
  !
  capital = in_char
  !
  RETURN
  !
END FUNCTION capital

!
!------------------------------------------------------------------------
FUNCTION int_to_char( i )
  !------------------------------------------------------------------------
  !
  IMPLICIT NONE
  !
  INTEGER, INTENT(in) :: i
  CHARACTER (len=8)   :: int_to_char
  !
  !
  IF ( i < 10 ) THEN
     !
     WRITE( UNIT = int_to_char , FMT = "(I1)" ) i
     !
  ELSEIF ( i < 100 ) THEN
     !
     WRITE( UNIT = int_to_char , FMT = "(I2)" ) i
     !
   ELSEIF ( i < 1000 ) THEN
     !
     WRITE( UNIT = int_to_char , FMT = "(I3)" ) i
     !
   ELSEIF ( i < 10000 ) THEN
     !
     WRITE( UNIT = int_to_char , FMT = "(I4)" ) i
     !
   ELSEIF ( i < 100000 ) THEN
     !
     WRITE( UNIT = int_to_char , FMT = "(I5)" ) i
     !
   ELSEIF ( i < 1000000 ) THEN
     !
     WRITE( UNIT = int_to_char , FMT = "(I6)" ) i
     !
   ELSE
     !
     WRITE( UNIT = int_to_char , FMT = "(I7)" ) i
     !
  ENDIF
  !
END FUNCTION int_to_char

