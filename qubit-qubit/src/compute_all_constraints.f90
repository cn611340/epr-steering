!
!
subroutine compute_all_constraints
  ! 
  use commvars, only: DP, eps, ndim, ndim1, iverbose, c3nui, nui, ui, rho, &
                      tempdir, ind3, lasym, p_lat, q_long, nvars, stdout, nthreads
  !
  implicit none
  !
  real(DP) :: a(3), b(3), c0, c(3), tmat(3,3), dd(nui), x(nvars), offset, deno, cn
  real(DP) :: vu(0:ndim), normal(1:ndim1)
  character(len=132) :: line, filename
  !
  integer*8 :: p
  integer :: i, io, j, k, l, iun
  integer, allocatable :: ncons(:)
  integer :: TID, OMP_GET_THREAD_NUM
  !
  character(len=8), external :: int_to_char
  !
  write(stdout,'(3x, "Setting up linear constraints ... ")',advance='no') 
  !
  ! write equality constraints to lp-format file
  !
  call write_equality_constraints( )
  !
  allocate( ncons(0:nthreads-1) ); ncons(0:nthreads-1) = 0
  !
  ! OPENMP parallelization
  !
!$OMP PARALLEL PRIVATE( TID, p, l, i, normal, x,  vu, filename ), &
!$OMP          PRIVATE( a, b, c0, c, tmat, offset, deno, cn, dd ), &
!$OMP          SHARED ( nthreads, nui, ui, ind3, c3nui, ncons ), &
!$OMP          SHARED ( iverbose, iun, tempdir )
  !
  a(1:3) = 2.d0*rho(0,1:3)
  b(1:3) = 2.d0*rho(1:3,0)
  tmat(1:3,1:3) = 2.d0*rho(1:3,1:3)
  tmat(1:3,1:3) = transpose( tmat(1:3,1:3) )
  ! setup OPENMP variables
  !
  TID = OMP_GET_THREAD_NUM()
  !
  !filename = "fort."//trim(int_to_char(TID+10))
  filename = trim(tempdir)//"/fort."//trim(int_to_char(tid+10))
  open(TID+10,file=trim(filename),status='replace',action='write' )
  !
!$OMP DO SCHEDULE(GUIDED) 
  !
  do p = 1, c3nui
    !
    if ( ind3(p,0).gt.0 ) cycle
    !
    ! normal vector to the plane (M1M2M3) in the Bloch hyperplane
    !call plane_normal_vect( M(1:3,1:ndim), normal(1:ndim1) )
    !write(*,*)ind3(p,1), ind3(p,2), ind3(p,3)
    call plane_normal_vect2( ui(0:ndim,ind3(p,1)), ui(0:ndim,ind3(p,2)), ui(0:ndim,ind3(p,3)), normal(1:ndim1) )
    c0 = -normal(ndim1); c(1:3) = normal(1:ndim)
    !
    ! determine UP and DOWN parts
    !
    dd(:) = 0.d0
    do l = 1, nui
      !
      !if ( (l.eq.ind3(p,1)) .or. (l.eq.ind3(p,2)) .or. (l.eq.ind3(p,3)) ) cycle
      ! dd(l) = c0 + c^Tn
      dd(l) = abs( dot_product( normal(1:ndim), ui(1:ndim,l) ) - normal(ndim1) )
      !
    enddo
    !
    ncons(tid) = ncons(tid) + 1
    ! each thread writes its constraints to a temp file
    !offset = c0 + dot_product(c(1:3),b(1:3))
    offset = 0.d0
    vu(1:3) = c0*a(1:3) + matmul(tmat(1:3,1:3),c(1:3))
    deno = sqrt(dot_product(vu(1:3),vu(1:3)))
    write(TID+10,'(A,SP,F23.12,A)') "tid"//trim(int_to_char(tid))// &
                  "c"//trim(int_to_char(ncons(tid)))//"up: ", -deno, " x0 "
    !
    if ( lasym  ) then
      !
      x(:) = eps(14) !0.d0
      do l = 1, nui
!        if ( dd(l).gt.0.d0 ) then
        if ( l.eq.nui ) then
          x(nvars) = x(nvars) + dd(l)
        else if ( l.eq.(nui-1) ) then
          x(nvars-1) = x(nvars-1) + dd(l)
        else if ( mod(l,p_lat).eq.0 ) then
          x(nvars-2) = x(nvars-2) + dd(l)
        else
          x(mod(l,p_lat)) = x(mod(l,p_lat)) + dd(l)
        endif
!        endif
      enddo
      !
      do i = 1, nvars
        if ( mod(i,4).eq.0 ) then
          write(TID+10,'(SP,F23.12,1x,A)') x(i), "x"//trim(int_to_char(i))
        else
          write(TID+10,'(SP,F23.12,1x,A)',advance='no') x(i), "x"//trim(int_to_char(i))
        endif
      enddo
      write(TID+10,'(A,F23.12)') " >= ", offset
      !
    else
      !
      i = 0
      do l = 1, nui
!        if ( dd(l).gt.0.d0 ) then
          i = i + 1
          if ( mod(i,4).eq.0 ) then
            write(TID+10,'(SP,F23.12,1x,A)') dd(l), "x"//trim(int_to_char(l))
          else
            write(TID+10,'(SP,F23.12,1x,A)',advance='no') dd(l), "x"//trim(int_to_char(l))
          endif
!        endif
      enddo
      write(TID+10,'(A,F23.12)') " >= ", offset
      !
    endif
    !
  enddo ! p loop
  !
!$OMP END DO NOWAIT
  !
  close(tid+10, status='keep')
  !
!$OMP END PARALLEL
  !
  deallocate( ncons )
  !
  ! main thread reads constraints that were written to separated files 
  ! by all threads in openmp parallelization and write them to optmzui.lp file
  !
  iun = 1; filename = trim(tempdir)//"/optmzui.lp"
  open( iun, file=trim(filename), status='unknown', position='append', &
        form='formatted', action='write' )
  !open( iun, file='optmzui.lp', status='unknown', position='append', action='write' )
  do i = 0, nthreads-1
    !
    filename = trim(tempdir)//"/fort."//trim(int_to_char(i+10))
    open(i+10,file=trim(filename),status='old')
    rewind(i+10)
    do
      read(i+10,'(A)',iostat=io) line
      if (io/=0) exit
      write(iun,'(A)') trim(line)
    enddo
    close(i+10, status='delete')
  enddo
  !
  ! write bound constraints to lp file
  write(iun,'(/,"bounds")')
  write(iun,'(" 0.00000000000000000 <= x0")')
  do i = 1, nvars
    write(iun,'(A)') " 0.00000000000000000 <= x"//trim(int_to_char(i))//" <= 1.000000000000000"
  enddo
  write(iun,'(/,"end",/)')
  !
  close(iun)
  !
  write(stdout,'(" done")')
  !
end subroutine compute_all_constraints
!
!
subroutine write_equality_constraints( )
  !
  use commvars,  only : DP, eps, ndim, nui, ui, tempdir, IAp, &
                        lasym, p_lat, q_long, nvars
  !
  implicit none
  !
  integer :: iun, i, j, k, l
  real(DP) :: x(nvars)
  character(len=132) :: filename
  character(len=8), external :: int_to_char
  !
  iun = 1; filename = trim(tempdir)//"/optmzui.lp"
  open( iun, file=trim(filename), status='unknown', form='formatted', action='write' )
  !
  write(iun,'(/,"Maximize")')
  write(iun,'("obj: x0")')
  !
  write(iun,'(/,"Subject To")')
  if ( lasym ) then
    ! 
    do k = 0, ndim 
      !
      write(iun,'(A)',advance='no')"IA"//trim(int_to_char(k))//": "
      !
      x(:) = eps(14) !0.d0
      do l = 1, nui
        if ( l.eq.nui ) then
          x(nvars) = x(nvars) + ui(k,l)
        else if ( l.eq.(nui-1) ) then
          x(nvars-1) = x(nvars-1) + ui(k,l)
        else if ( mod(l,p_lat).eq.0 ) then
          x(nvars-2) = x(nvars-2) + ui(k,l)
        else
          x(mod(l,p_lat)) = x(mod(l,p_lat)) + ui(k,l)
        endif
      enddo
      !
      do i = 1, nvars
        if ( mod(i,4).eq.0 ) then
          write(iun,'(SP,F23.12,1x,A)') x(i), "x"//trim(int_to_char(i))
        else
          write(iun,'(SP,F23.12,1x,A)',advance='no') x(i), "x"//trim(int_to_char(i))
        endif
      enddo
      !
      write(iun,'(A,F23.12)') " = ", IAp(k)
      !
    enddo
    ! 
  else
    !
    !write(iun,'(/,"Subject To")')
    do k = 0, ndim
      !
      write(iun,'(A)',advance='no')"IA"//trim(int_to_char(k))//": "
      do i = 1, nui
        if ( abs(ui(k,i)).lt.1.d-14 ) ui(k,i) = 1.d-14
        if ( mod(i,4).eq.0 ) then
          write(iun,'(SP,F20.10,A)') ui(k,i), " x"//trim(int_to_char(i)) 
        else
          write(iun,'(SP,F20.10,A)',advance='no') ui(k,i), " x"//trim(int_to_char(i)) 
        endif
      enddo
      write(iun,'(A,F20.10)') " = ", IAp(k)
      !
    enddo
    !
  endif
  !
  close(iun, status='keep')
  !
  return
  !
end subroutine write_equality_constraints
