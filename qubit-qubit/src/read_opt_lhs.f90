subroutine read_opt_lhs( )
  !
  ! ui: ensemble of LHV
  ! u: distribution function over ui
  !
  use commvars, only : DP, ndim, nui, nvars, dmu, lasym, r_u_lp, &
                       p_lat, q_long, stdout
  !
  implicit none
  !
  integer :: i, j
  !
  !write(*,'(3x,"- weights read from file",/)') 
  !
  open( 1, file="weights.txt", status="old", action="read" )
  rewind(1)
  !
  read(1,*) r_u_lp
  !write(stdout,'(5x,"r_u_lp = ",F17.12)') r_u_lp
  !
  if ( allocated( dmu ) ) deallocate( dmu )
  allocate( dmu(nui) ); dmu(:) = 0.d0
  do i = 1, nvars
    read(1,*) dmu(i)
  enddo
  !
  close(1)
  !
  if ( lasym  ) then
    !
    dmu(nui) = dmu(nvars)
    dmu(nui-1) = dmu(nvars-1)
    !
    do j = 1, q_long-1
      do i = 1, p_lat
        dmu(j*p_lat+i) = dmu(i)
      enddo
    enddo
  endif
  !
  call check_minreq_lhs( )
  !
  return
  !
end subroutine read_opt_lhs
!
!
subroutine check_minreq_lhs( )
  !
  use commvars,  only: DP, eps, ndim, nui, ui, dmu, IAp, lbloch, stdout
  !
  implicit none
  !
  real(DP) :: u, sv(0:ndim)
  integer :: i, j
  !
  ! check to make sure that all the points are on Bob's positive cone ...
  ! and the principle vertex is at IAp 
  !
  sv(:) = 0.d0
  do i = 1, nui
    !
    if ( lbloch ) then
      u = ui(0,i)**2 - dot_product( ui(1:ndim,i),ui(1:ndim,i) )
      if ( abs(u).gt.eps(8) ) then
        write(stdout,*)"Not on Bob's positive cone", i
        stop
      endif
    endif
    !
    sv(0:3) = sv(0:3) + dmu(i)*ui(0:3,i)
    !
  enddo
  !
  !if ( dot_product(sv(1:3), sv(1:3)).gt.eps8 .or. &
  !     abs(1.d0-sv(0)).gt.eps8  ) then
  sv(:) = sv(:) - IAp(:)
  if ( dot_product(sv(0:3),sv(0:3)).gt.eps(4) ) then
    write(stdout,*) "init_weights: minimal requirement not satisfied"
    write(stdout,*) sv(0), dot_product(sv(1:3), sv(1:3))
    stop
  endif
  !
end subroutine check_minreq_lhs
