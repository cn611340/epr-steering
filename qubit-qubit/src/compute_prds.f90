!
!
subroutine compute_prds( )
  ! 
  use commvars, only: DP, eps, ndim, ndim1, iverbose, c3nui, nui, ui, rho, &
                      ind3, p_lat, q_long, nvars, dmu, r_u, stdout, nthreads
  !
  implicit none
  !
  real(DP) :: a(3), b(3), c0, c(3), tmat(3,3), dd 
  real(DP) :: vu(0:ndim), normal(1:ndim1)
  real(DP) :: prds
  real(DP), allocatable :: prds_global(:)
  !
  integer*8 :: p
  integer :: i, j, k, l
  integer :: TID, OMP_GET_THREAD_NUM
  !
  !write(stdout,'(/,3x, "Compute r_u using the formula in ALICE space ... ")') 
  ALLOCATE( prds_global(0:NTHREADS-1) ); prds_global(0:NTHREADS-1) = 10000000.d0
  !
  ! OPENMP parallelization
  !
!$OMP PARALLEL PRIVATE( TID, p, l, i, normal, vu ), &
!$OMP          PRIVATE( a, b, c0, c, tmat, dd, prds ), &
!$OMP          SHARED ( NTHREADS, nui, ui, ind3, c3nui, iverbose )
  !
  a(1:3) = 2.d0*rho(0,1:3)
  b(1:3) = 2.d0*rho(1:3,0)
  tmat(1:3,1:3) = 2.d0*rho(1:3,1:3)
  tmat(1:3,1:3) = transpose( tmat(1:3,1:3) )
  !
  prds = 1.d+6
  TID = OMP_GET_THREAD_NUM()
  !
!$OMP DO SCHEDULE(GUIDED) 
  !
  do p = 1, c3nui
    !
    if ( ind3(p,0).gt.0 ) cycle
    !
    ! normal vector to the plane (M1M2M3) in the Bloch hyperplane
    !call plane_normal_vect( M(1:3,1:ndim), normal(1:ndim1) )
    !write(*,*)ind3(p,1), ind3(p,2), ind3(p,3)
    call plane_normal_vect2( ui(0:ndim,ind3(p,1)), ui(0:ndim,ind3(p,2)), ui(0:ndim,ind3(p,3)), normal(1:ndim1) )
    c0 = -normal(ndim1); c(1:3) = normal(1:ndim)
    !
    ! determine UP and DOWN parts
    !
    dd = 0.d0
    do l = 1, nui
      !
      !if ( (l.eq.ind3(p,1)) .or. (l.eq.ind3(p,2)) .or. (l.eq.ind3(p,3)) ) cycle
      ! dd(l) = c0 + c^Tn
      dd = dd + dmu(l)*abs( dot_product( normal(1:ndim), ui(1:ndim,l) ) - normal(ndim1) )
      !
    enddo
    !
    vu(1:3) = c0*a(1:3) + matmul(tmat(1:3,1:3),c(1:3))
!if (dd.lt.eps(8)) then
!write(*,*) ds
!write(*,*) normal
!!write(*,*) dmu(:)
!write(*,*)dd,sqrt(dot_product(vu(1:3),vu(1:3))), dd/sqrt(dot_product(vu(1:3),vu(1:3)))
!endif
    !if ( sqrt(dot_product(vu(1:3),vu(1:3))).lt.eps(12) ) then
    if ( dot_product(vu(1:3),vu(1:3)).lt.eps(12) ) then
      dd = 1.d+10
    else
      dd = dd / sqrt(dot_product(vu(1:3),vu(1:3)))
    endif
    !
    if ( dd.lt.prds ) prds = dd
    !
  enddo ! p loop
  !
!$OMP END DO NOWAIT
  !
  prds_global(TID) = prds
  !
!$OMP END PARALLEL
  !
  r_u = minval(prds_global(0:)); deallocate( prds_global )
  !
  !if (iverbose.ne.0) & 
  !  write(stdout,'(3x,A,I5,f18.12)') "PRINCIPAL RADIUS r_u: ", nui, r_u
  !
end subroutine compute_prds
