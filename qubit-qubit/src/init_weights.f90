subroutine init_weights( )
  !
  ! ui: ensemble of LHV
  ! u: distribution function over ui
  !
  use random_numbers,  only : randy, set_random_seed
  use commvars, only : DP, eps, IAp, lrandom, linvsym, ndim, nui, &
                       ui, uia, dmu, rho, invrho, winit, inv4ui, &
                       lcheck_lp, loptmzui, lcylsym, nvars, r_u_lp, &
                       p_lat, q_long, lpole, llebedev, wleb, wgl, &
                       ilat_type, ds, lwrescl, wres
  !
  implicit none
  !
  real(DP) :: u, sv(0:ndim), tmat(3,3), tmatm1(3,3), su(0:ndim,p_lat)
  real(DP) :: a1, a2, b1, b2, c1, c2, d, dx, dy
  integer, allocatable :: ind(:)
  integer :: i, i0, j, k, l, nui_
  logical :: lbounded
  real(DP), allocatable :: dmu_(:), ui_(:,:)
  !
  character(len=1), external :: capital 
  character(len=50) :: winitout
  !
  !call set_random_seed( )
  !
  ! convert winit to uppercase
  !
  k = len_trim(winit)
  winitout = ' '
  do l = 1, k
    winitout (l:l) = capital ( winit(l:l) )
  enddo
  !
  if ( allocated( dmu ) ) deallocate( dmu )
  allocate( dmu(nui) ); dmu(1:nui) = 0.d0
  !
  if ( trim(winitout) .eq. 'UNIFORM' ) then
    !
    dmu(1:nui) = 1.d0/dble(nui)
    !
  endif
  !
  if ( trim(winitout).eq.'RANDOM' ) then
    !
    if ( lasym ) then 

    else
      !
      call random_number( dmu )
      dmu(1:nui) = dmu(1:nui) / sum(dmu(1:nui))
      !
    endif allocated( dmu ) ) deallocate( dmu )
    allocate( dmu(nui) ); dmu(:) = 0.d0
    !
    u = dot_product( IAp(1:3), IAp(1:3) )
    if ( abs(u).gt.eps(6) ) then
      dmu(nui)   = (IAp(0) + IAp(3)) / 2.d0 ! NB: ui(:,nui) =   (1,0,0, 1)
      dmu(nui-1) = (IAp(0) - IAp(3)) / 2.d0 !     ui(:,nui-1) = (1,0,0,-1)
    else
      dmu(1:nui) = 1.d0/dble(nui)
    endif
    !
    dmu(1:nui) = dmu(1:nui) /  ds
    call check_normalization_lhs( )
    !
    return
!    do i = 1, p_lat
!      if ( abs(ui(3,i)).lt.eps(8) ) then
!        i0 = i; exit
!      endif
!    enddo
    !
!    su(:,:) = 0.d0
!    do i = 1, nui
!      k = mod(i,p_lat)
!      if ( mod(i,p_lat).eq.0 ) then
!        su(0:ndim,p_lat) = su(0:ndim,p_lat) + ui(0:ndim,p_lat)
!      else
!        su(0:ndim,k) = su(0:ndim,k) + ui(0:ndim,i)
!      endif
!    enddo
!    sv(0:ndim) = su(0:ndim,1)
!    su(0:ndim,1) = su(0:ndim,i0)
!    su(0:ndim,i0) = sv(0:ndim)
    !
!    lbounded = .false.; k = 1
!    do while ( .not.lbounded )
!      !
!      if ( k.ge.p_lat ) exit
!      !
!      k = k + 1; dmu(1:p_lat) = eps(4)
!write(*,'(4(F17.12))')su(:,1)
!write(*,'(4(F17.12))')su(:,k)
!      dmu(1) = 0.d0; dmu(k) = 0.d0
!      do i = 2, p_lat
!        if ( i.ne.k ) then
!          dmu(1) = dmu(1) + dmu(i)*su(0,i)
!          dmu(k) = dmu(k) + dmu(i)*su(3,i)
!        endif
!      enddo
!      dmu(k) = (IAp(3) - dmu(k))/su(3,k)
!      dmu(1) = (IAp(0) - dmu(1) - dmu(k)*su(0,k))/su(0,1)
!      write(*,'(2(F17.12),/)') dmu(1), dmu(k)
!      !
!      lbounded = (dmu(1).ge.0.d0) .and. (dmu(1).le.1.d0) .and. &
!                 (dmu(k).ge.0.d0) .and. (dmu(k).le.1.d0)
!    enddo
    !
!    if ( .not.lbounded ) then
!      write(*,*)"weights are not bounded: ", dmu(1), dmu(2)
!      !stop
!    else
!      u = dmu(i0); dmu(i0) = dmu(1); dmu(1) = u
!    endif
    !
    do j = 1, q_long-1
      do i = 1, p_lat
        dmu(j*p_lat+i) = dmu(i)
      enddo
    enddo
    if ( lbounded ) write(*,*) dmu(:)

!    call write_equality_constraints( .true. ) 
!    !
!    call cplex_lp_optimization( )
!    !
!    open( 1, file="weights.txt", status="old", action="read" )
!    rewind(1); read(1,*)
!    !
!    if ( allocated( dmu ) ) deallocate( dmu )
!    allocate( dmu(nui) ); dmu(:) = 0.d0
!    do i = 1, nvars
!      read(1,*) dmu(i)
!    enddo
!    close(1)
!    !
!    if ( lcylsym .and. (nvars.ne.nui) ) then
!      if ( lpole ) then
!        dmu(nui) = dmu(nvars)
!        dmu(nui-1) = dmu(nvars-1)
!      endif
!      do j = 1, q_long-1
!        do i = 1, p_lat
!          dmu(j*p_lat+i) = dmu(i)
!        enddo
!      enddo
!    endif
!    !
    !call check_normalization_lhs( )
    !
    return
    !
  endif
  !
  if ( trim(winitout).eq.'WEIGHTS4PRDS' ) then
    !
    if ( .not.lcylsym ) then
      write(*,'("WEIGHTS4PRDS ONLY WORKS WITH lcylsym=.true.")')
      stop
    endif
    !
    write(*,'(3x,"- weights read from file with less points")') 
    open( 1, file="weights4prds.txt", status="old", action="read" )
    rewind(1)
    !
    read(1,*)nui_, r_u_lp
    write(*,'(5x,"- nui (input) = ",I5,3x,"r_u_lp (input) = ",F17.12)') nui_, r_u_lp
    !
    if ( allocated( dmu ) ) deallocate( dmu )
    allocate( dmu(nui) ); dmu(:) = 0.d0
    !
    allocate( dmu_(nui_), ui_(0:ndim,nui_) )
    do i = 1, nui_
      read(1,*) dmu_(i), ui_(0:ndim,i)
      do j = 1, nui
        sv(0:ndim) = ui_(0:ndim,i) - ui(0:ndim,j)
        if ( dot_product(sv(0:ndim),sv(0:ndim)).gt.eps(8) ) then
          write(*,'("OLD POINTS ARE NOT A SUBSET OF NEW POINTS")')
          stop
        else
          dmu(j) = dmu_(i)
        endif
      enddo
    enddo
    close(1)
    !
    deallocate( dmu_, ui_ )
    !
    call check_normalization_lhs( )
    !
    return
    !
  endif
  !
  !
  if ( trim(winitout).eq.'FILE' ) then
    !
    write(*,'(3x,"- weights read from file",/)') 
    open( 1, file="weights.txt", status="old", action="read" )
    rewind(1)
    !
    if ( lcheck_lp .or. loptmzui ) then
      read(1,*) r_u_lp
      write(*,'(5x,"r_u_lp = ",F17.12)') r_u_lp
    else
      read(1,*) k
      if ( k.ne.nvars ) then
        write(*,*)"init_weights: wrong number of points"
        stop
      endif
    endif
    !
    if ( allocated( dmu ) ) deallocate( dmu )
    allocate( dmu(nui) ); dmu(:) = 0.d0
    do i = 1, nvars
      read(1,*) dmu(i)
    enddo
    close(1)
    !
    !if ( lcylsym ) then
    if ( lcylsym .and. (nvars.ne.nui) ) then
      if ( lpole ) then
        dmu(nui) = dmu(nvars)
        dmu(nui-1) = dmu(nvars-1)
      endif
      do j = 1, q_long-1
        do i = 1, p_lat
          dmu(j*p_lat+i) = dmu(i)
        enddo
      enddo
    endif
    !
!    if ( lwrescl ) then
!      dmu(1:nui) = dmu(1:nui)*wres(1:nui)
!      dmu(1:nui) = dmu(1:nui) / sum( dmu(1:nui) )
!    endif
    !
    call check_normalization_lhs( )
    !
    return
    !
  endif
  !
  ! if weights are to be generated and ui's are truly random points 
  ! which have no symmetry, their weights were already taken care of 
  ! in the subroutine that generates the points 
  !
  if (lrandom) return
  !
  !
  if ( trim(winitout).eq.'RANDOM' ) then
    !
    write(*,'(3x,"- random weights",/)') 
    !
    if ( allocated( dmu ) ) deallocate( dmu )
    allocate( dmu(nui) ); dmu(:) = 0.d0
    !
    ! the piece of code that was commented will modify
    ! the coordiantes of the last point in the array ui 
    !sv(:) = 0.d0
    !do i = 1, nui-1
    !  dmu(i) = randy()
    !  sv(1:3) = sv(1:3) + dmu(i)*ui(1:3,i)
    !enddo
    !dmu(nui) = sqrt( dot_product(sv(1:3), sv(1:3)) )
    !ui(1:3,nui) = -sv(1:3) / dmu(nui)
    !dmu(:) = dmu(:) / sum(dmu(1:nui))
    !
    ! two points that transform to each other under the 
    ! inversion symmetry will have the same weight
    u = dot_product( IAp(1:3), IAp(1:3) )
    if ( linvsym .and. (u.lt.eps(12)) ) then
      !
      allocate( ind(nui) ); ind(:) = 0
      do i = 1, nui
        if ( ind(i).ne.0 ) cycle
        dmu(i) = randy(); ind(i) = 1; k = i
        do j = i+1, nui
          if ( ind(j).lt.0 ) then
            cycle
          else
            u = dot_product( ui(1:ndim,i),ui(1:ndim,j) ) 
            if ( u.lt.0 .and. abs(1.d0-abs(u)).lt.eps(6) ) then
              dmu(j) = dmu(i); ind(j) = -1; l = j
              !write(*,*)ui(1:ndim,i)
              !write(*,*)ui(1:ndim,j)
              !write(*,*)"-----------"
              exit
            endif
          endif
        enddo
      enddo
      !
      !
      dmu(:) = dmu(:) / sum(dmu(:))
      sv(0) = 0.d0
      do i = 1, nui
        if ( i.eq.k .or. i.eq.l ) cycle
        sv(0) = sv(0) + dmu(i)
      enddo
      dmu(k) = (1.d0 - sv(0) )/2.d0
      dmu(l) = dmu(k) 
      !
    else
      !
      allocate( ind(nui) ); ind(:) = 0
      do i = 1, nui
        if ( ind(i).ne.0 ) cycle
        ind(i) = 1; k = i
        do j = i+1, nui
          if ( ind(j).lt.0 ) then
            cycle
          else
            u = dot_product( ui(1:ndim,i),ui(1:ndim,j) ) 
            if ( u.lt.0 .and. abs(1.d0-abs(u)).lt.eps(6) ) then
              dmu(i) = randy(); dmu(j) = dmu(i); 
              ind(j) = -1; l = j
              !write(*,*)ui(1:ndim,i)
              !write(*,*)ui(1:ndim,j)
              !write(*,*)"-----------"
              exit
            endif
          endif
        enddo
      enddo
      !
      u = dot_product( IAp(1:3), IAp(1:3) )
      if ( sum(dmu(:)).ne.0.d0 .and. (u.lt.eps(12)) ) then
        !
        dmu(:) = dmu(:) / sum(dmu(:))
        write(*,*) "Weights are non-zero only for points with inversion partners"
        !
      else
        !
        call set_random_seed( )
        !
        write(*,*) "No single pair of points with inversion symmetry"
        lbounded = .false.
        do while ( .not.lbounded )
          !
          k = 0; ind(1:4) = 0
          do i = nui-4+1, nui
            l = i*randy(); if ( l.eq.0 ) l = l +1
            k = k + 1
            if ( l.ne.ind(1) .and. l.ne.ind(2) .and. & 
                 l.ne.ind(3) .and.  l.ne.ind(4) ) then 
              ind(k) = l
            else
              ind(k) = i
            endif
          enddo
          !write(*,*) ind(1:4)
          !
          do i = 1, 4
             inv4ui(0:ndim,i-1) = ui(0:ndim,ind(i))
          enddo
!          call dgminv( 4, 4, inv4ui(0:ndim,0:ndim) )
          !
          lbounded = (inv4ui(0,0).gt.0.d0) .and. (inv4ui(0,0).lt.1.d0) .and. &
                     (inv4ui(1,0).gt.0.d0) .and. (inv4ui(1,0).lt.1.d0) .and. &
                     (inv4ui(2,0).gt.0.d0) .and. (inv4ui(2,0).lt.1.d0) .and. &
                     (inv4ui(3,0).gt.0.d0) .and. (inv4ui(3,0).lt.1.d0)
          !if ( lbounded ) write(*,*) inv4ui(0,0), inv4ui(1,0), inv4ui(2,0), inv4ui(3,0)
          ! 
        enddo
        !
        do i = 1, 4
          dmu(ind(i)) = inv4ui(i-1,0)
        enddo
        !sv(0:3) = 0
        !do i = 1, nui
        !  if ( dmu(i).gt.0.d0 ) write(*,*) i, dmu(i) 
        !  if ( dmu(i).gt.0.d0 ) sv(0:3) = sv(0:3) + dmu(i)*ui(0:3,i) 
        !enddo
        !write(*,*) sv(0:3)
        !stop
        !
        dmu(:) = dmu(:) / sum(dmu(:))
        write(*,*) "Weighs are non-zero for 4 suitably-chosen points"
        !
      endif
      !
    endif
    !
  else if ( trim(winitout).eq.'UNIFORM' ) then
    !
    write(*,'(3x,"- uniform weights",/)') 
    !
    if ( allocated( dmu ) ) deallocate( dmu )
    allocate( dmu(nui) );  dmu(1:nui) = 1.d0
    !
    if ( lwrescl ) then
      dmu(1:nui) = dmu(1:nui)*wres(1:nui)
      dmu(1:nui) = dmu(1:nui) / sum( dmu(1:nui) )
    endif
    !
!    allocate( wres(nui) ); wres(1:nui) = 1.d0 
!    if ( llebedev .and. lwrescl ) wres(1:nui) = wleb(1:nui)
!    !
!    if ( lcylsym .and. gl_theta .and. lwrescl) then 
!      nui_ = nui; if ( lpole ) nui_ = nui - 2
!      do i = 1, nui_
!        if ( mod(i,p_lat).eq.0 ) then
!          wres(i) = wgl(p_lat)
!        else
!          wres(i) = wgl(mod(i,p_lat))
!        endif
!      enddo
!    endif  
!    !
!    do i = 1, nui
!      if ( wres(i).gt.0.d0 ) then
!        dmu(i) = dmu(i)*wres(i)
!      else
!        write(*,*)"init_weight: wleb < 0", i
!        stop
!      endif
!    enddo
!    dmu(1:nui) = dmu(1:nui) / sum( dmu(1:nui) )
!    !dmu(:) = dmu(:) / sum(dmu(:))
!    deallocate( wres)
!    if ( allocated(wleb) ) deallocate( wleb )
!    if ( allocated(wgl) ) deallocate( wgl )
    !
  else if ( trim(winitout).eq.'T-STATES' ) then
    !
    write(*,'(3x,"- T-states weights",/)')
    !
    if ( allocated( dmu ) ) deallocate( dmu )
    allocate( dmu(nui) )
    tmat(1:3,1:3) = rho(1:3,1:3)
    tmatm1(1:3,1:3) = 0.d0
    tmatm1(1,1) = 1.d0/tmat(1,1)
    tmatm1(2,2) = 1.d0/tmat(2,2)
    tmatm1(3,3) = 1.d0/tmat(3,3)
    do i = 1, nui
      sv(1:3) = ui(1:3,i)
      sv(1:3) = matmul( tmatm1(1:3,1:3), sv(1:3) )
      sv(1:3) = matmul( tmatm1(1:3,1:3), sv(1:3) )
      !dmu(i) = 1.d0 / dot_product( sv(1:3), sv(1:3) )
      dmu(i) = 1.d0 / (dot_product( ui(1:3,i), sv(1:3) ))**2
  !write(*,'(4(F17.12))') ui(1:3,i), dmu(i)
      if ( lcylsym .and. (ilat_type.eq.0) ) &
        dmu(i) = dmu(i)*sin( acos(ui(3,i)) ) 
    enddo
!stop
    !
    if ( lwrescl ) dmu(1:nui) = dmu(1:nui)*wres(1:nui)
    dmu(1:nui) = dmu(1:nui) / sum( dmu(1:nui) )
    dmu(1:nui) = dmu(1:nui) / ds
    !
!    allocate( wres(nui) ); wres(1:nui) = 1.d0 
!    !
!    if ( llebedev .and. lwrescl ) wres(1:nui) = wleb(1:nui)
!    !
!    if ( lcylsym .and. gl_theta .and. lwrescl) then 
!      nui_ = nui; if ( lpole ) nui_ = nui - 2
!      do i = 1, nui_
!        if ( mod(i,p_lat).eq.0 ) then
!          wres(i) = wgl(p_lat)
!        else 
!          wres(i) = wgl(mod(i,p_lat))
!        endif
!      enddo
!    endif  
!    !
!    do i = 1, nui
!      if ( wres(i).gt.0.d0 ) then
!        dmu(i) = dmu(i)*wres(i)
!      else
!        write(*,*)"init_weight: wleb or wgl < 0", i
!        stop
!      endif
!    enddo
!    dmu(1:nui) = dmu(1:nui) / sum( dmu(1:nui) )
!    !
!    deallocate( wres )
!    !
  else
    !
    write(*,'("init_weights: unknown type of init weights")')
    !
  endif
  !
  call check_normalization_lhs( )
  !
  return
  !
end subroutine init_weights
!
!
subroutine check_normalization_lhs( )
  !
  use commvars,  only: DP, eps, ndim, nui, ui, dmu, ds, IAp, lbloch
  !
  implicit none
  !
  real(DP) :: u, sv(0:ndim)
  integer :: i, j
  !
  ! check to make sure that all the points are on Bob's positive cone ...
  ! and the principle vertex is at IAp 
  !
  sv(:) = 0.d0
  do i = 1, nui
    !
    if ( lbloch ) then
      u = ui(0,i)**2 - dot_product( ui(1:ndim,i),ui(1:ndim,i) )
      if ( abs(u).gt.eps(8) ) then
        write(*,*)"Not on Bob's positive cone", i
        stop
      endif
    endif
    !
    sv(0:3) = sv(0:3) + ds*dmu(i)*ui(0:3,i)
    !
  enddo
  !
  !if ( dot_product(sv(1:3), sv(1:3)).gt.eps8 .or. &
  !     abs(1.d0-sv(0)).gt.eps8  ) then
  sv(:) = sv(:) - IAp(:)
  if ( dot_product(sv(0:3),sv(0:3)).gt.eps(4) ) then
    write(*,*) "init_weights: minimal requirement not satisfied"
    write(*,*) sv(0), dot_product(sv(1:3), sv(1:3))
    stop
  endif
  !
end subroutine check_normalization_lhs
!
!
subroutine rescale_weights( )
  !
  use commvars, only : DP, nui, wleb, wgl, wres, p_lat, &
                       lpole, llebedev, lcylsym, ilat_type
  !
  implicit none
  !
  integer :: i, nui_
  !
  if ( llebedev ) wres(1:nui) = wleb(1:nui)
  !
  if ( lcylsym .and. ilat_type ) then 
    nui_ = nui; if ( lpole ) nui_ = nui - 2
    do i = 1, nui_
      if ( mod(i,p_lat).eq.0 ) then
        wres(i) = wgl(p_lat)
      else
        wres(i) = wgl(mod(i,p_lat))
      endif
    enddo
  endif  
  !
  do i = 1, nui
    if ( wres(i).lt.0.d0 ) then
      write(*,*)"init_weight: wleb or wgl < 0", i
      stop
    endif
  enddo
  !dmu(1:nui) = dmu(1:nui) / sum( dmu(1:nui) )
  !
  if ( allocated(wleb) ) deallocate( wleb )
  if ( allocated(wgl) ) deallocate( wgl )
  !
  return
  !
end subroutine rescale_weights
