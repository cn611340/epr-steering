subroutine readin
  !
  use commvars, only : DP, eps, nui, ndim, ndim1, rho, cplexmem, steering, &
                       stdout, lasym, p_lat, q_long, lbloch, lb0, r_in, &
                       calculation, iverbose, tempdir, cplexdir, nthreads, &
                       thetas, nstates, infile, outfile, pathfile, state_id
  !
  implicit none
  !
  integer :: ios, io
  integer :: narg, ntab, i, j, k, l
  character(len=1) :: r
  character(len=80) :: card, arg
  character(len=400) :: line
  !
  integer :: tid, omp_get_num_threads, omp_get_thread_num, omp_get_max_threads
  !
  !namelist / input / calculation, tempdir, cplexdir, &
  !                   lasym, nui, lb0, rho, cplexmem
  namelist / dirs / tempdir, cplexdir
  !
  narg = command_argument_count()
  !
  i = 0
  do while ( i.lt.narg) 
    !
    i = i + 1; call get_command_argument(i, arg)
    if ( trim(arg).eq.'-o' .or. trim(arg).eq.'-O' .or. &
         trim(arg).eq.'-out' .or. trim(arg).eq.'-output' ) then
      i = i + 1; stdout = 96
      call get_command_argument(i, outfile)
      open( stdout, file=outfile, status='unknown', form='formatted', action='write' )
    endif
  enddo
  !
  !
  WRITE( stdout, '(/5X,"This program computes the critical radius of a quantum state,",&
            & /5X,"a quantity that can be used to determine its EPR steerability.",/,&
            & /5X,"For details of the theory, please refer to: ", &
            & /7X,"H. C. Nguyen, H.-V. Nguyen, and O. Guehne, arXiv:1808.09349 (2018).",&
            & /5X,"If you encounter problems or have suggestions, contact us at:", &
            & /7X,"HCN (chaunguyen2208@gmail.com) and HNV (hviet.nguyen@gmail.com)" )')
  !         
!$OMP PARALLEL SHARED( nthreads ) PRIVATE( tid) 
  tid = omp_get_thread_num( )
  if ( tid .eq. 0 ) nthreads = omp_get_max_threads( )
!$OMP END PARALLEL
  write(stdout,'(/5X,"The program runs with ",I2," OPENMP threads")') nthreads
  !
  if ( narg .gt. 12 ) then
    write(stdout,'(A,I3)')"readin: too many arguments! ", narg
    stop
  endif
  !
  ! set default values
  !
  calculation = 'R_bounds'; tempdir = ''; cplexdir = ''
  nui = 92; lasym = .false.; lb0 = .true.; cplexmem = 32000
  steering = 'AB'
  !
  !
  i = 0
  do while ( i .lt. narg )
    !
    i = i + 1
    call get_command_argument(i, arg)
    !
    select case (arg)
      !
      case ( '-n', '-N' )
        i = i + 1
        call get_command_argument(i, arg)
        !READ ( arg, *, ERR = 15, END = 15) nui_ 
        read ( arg, * ) nui 
        if ( (nui.eq.114)  .or. (nui.eq.182)  .or. &
             (nui.eq.266)  .or. (nui.eq.366)  .or. &
             (nui.eq.482)  .or. (nui.eq.614)  .or. &
             (nui.eq.762)  .or. (nui.eq.926)  .or. & 
             (nui.eq.1106) .or. (nui.eq.1302) .or. &
             (nui.eq.1514)  ) then
          lasym = .true.
          !write(stdout,'(/5X,"The Bloch sphere is approximated by ",I4,"-vertex polytopes")')nui
          !write(stdout,'( 5X,"WARNING: this polytope is valid for states with AXIAL SYMMETRY only!!!")')
          !write(stdout,'( 5X,"         PLEASE VERIFY THAT THIS IS WHAT YOU REALLY WANT!!!")')
          !
        elseif  ( (nui.eq.92) .or. (nui.eq.162) .or. (nui.eq.252) ) then
          !
          lasym = .false.
          !write(stdout,'(/5X,"The Bloch sphere is approximated by ",I4,"-vertex polytopes")')nui
          !
        else
          !
          write(stdout,'(/,"###################### ERROR MESSAGE ######################")')
          write(stdout,'(2X,"readin: wrong number of polytope vertices")')
          write(stdout,'(4X,"AXIAL states:   N = 114, 182, 266, 366, 482, 614,",/,&
                       &"                        762, 926, 1106, 1302, 1514")')
          write(stdout,'(4X,"GENERIC states: N = 92, 162, 252")')
          write(stdout,'("###########################################################")')
          stop
          !
        endif
        !
      case ( '-i', '-I', '-in', '-inp', '-input' )
        i = i + 1
        call get_command_argument(i, infile)
        !print*, "input file = ", trim(infile)
        !
      case ( '-o', '-O', '-out', '-output' )
        i = i + 1
        call get_command_argument(i, outfile)
        !stdout = 96
        !open( stdout, file=outfile, status='unknown', form='formatted', action='write' )
        !print*, "output file = ", trim(outfile)
        !
      case ( '-p', '-path' )
        i = i + 1
        call get_command_argument(i, pathfile)
        !print*, "path file = ", trim(pathfile)
        !
      case ( '-r', '-R' )
        i = i + 1
        call get_command_argument(i, r)
        if ( r.eq.'+' ) then
          calculation = 'R_upper'
        elseif ( r.eq.'-' ) then 
          calculation = 'R_lower'
        endif
        !
      case ( '-steering', '-s', '-S' )
        i = i + 1
        call get_command_argument(i, steering)
        !
      case default
        write(stdout,'(/,"###################### ERROR MESSAGE ######################")')
        write(stdout,'(2X,"readin: unexpected argument")')
        write(stdout,'("###########################################################")')
        stop
        !
    end select
    !
  enddo 
  !
  ! read paths to cplex and temp. directories
  !
  open( 1, file = pathfile, status = 'old', action = 'read' )
  read (1, dirs, iostat = ios)
  if ( ios/=0 ) then
    write(stdout,'(/,"###################### ERROR MESSAGE ######################")')
    write(stdout,'(2X,"readin: problem when reading pathfile")')
    write(stdout,'("###########################################################")')
    stop
  endif
  write(stdout,'(/5X,"tempdir: ",A)') tempdir
  write(stdout,'(/5X,"cplexdir: ",A)') cplexdir
  !
  ! read states
  !
  open( 1, file = infile, status = 'old', action = 'read' )
  !
  rewind(1); nstates = 0
  do
    read(1,*,iostat=io)
    if (io/=0) exit
    nstates = nstates + 1
  enddo
  !
  allocate( thetas(16,nstates), state_id(nstates) ); 
  !
  rewind(1)
  do i = 1, nstates
    !
    read(1,'(A)') line;
    ! remove whitespace and tabs before and after input datat
    line = trim(adjustl(line))
    l=1; do while ( line(l:l).eq.achar(9) ); line(l:l)=' '; l=l+1; enddo
    l=len(trim(line)); do while ( line(l:l).eq.achar(9) ); line(l:l)=' '; l=l-1; enddo
    ! count the number of tabs
    ntab = 0; line = trim(adjustl(line))
    do l = 1, len(trim(line))
      if ( line(l:l) .eq. achar(9) ) ntab = ntab + 1
    enddo
    !
    ! Assign data to Theta matrix
    !
    select case (ntab)
      !
      case (16)
        !
        k = 0; j = -1; arg = ''
        do l = 1, len(trim(line))
          if ( line(l:l).ne.achar(9) ) then
            k = k + 1; arg(k:k) = line(l:l)
          else
            j = j + 1
            if ( j.eq.0 ) then
              state_id(i) = trim(arg)
            else
              read(arg,*)thetas(j,i)
            endif 
            k = 0; arg = ''
          endif
        enddo
        j=j+1; read(arg,*)thetas(j,i) ! last element
        !
      case (15)
        !
        state_id(i) = 'not_present'
        !
        k = 0; j = 0; arg = ''
        do l = 1, len(trim(line))
          if ( line(l:l).ne.achar(9) ) then
            k = k + 1; arg(k:k) = line(l:l)
          else
            j = j + 1; read(arg,*)thetas(j,i) 
            k = 0; arg = ''
          endif
        enddo
        j=j+1; read(arg,*)thetas(j,i) ! last element
        !
      case default
        !
        state_id(i) = 'invalid'
      !
    end select
    !
    if ( abs(thetas(1,i)-1.d0).gt.eps(8) ) state_id(i) = 'invalid'
    !
    !write(stdout,'(/5X,"State with state_id = ",A)')state_id(i)
    !do l = 1, 4
    !  write(stdout,'(4(F17.12))') thetas(l,i), thetas(l+4,i),thetas(l+8,i),thetas(l+12,i)
    !enddo
    ! 
  enddo
  close (1)
  !
  ! THETA_MATRIX readin
  !
  !card = ''
  !do while ( trim(card).ne.'THETA_MATRIX' )
  !  read(5,*)card
  !enddo
  !read(5,*)rho(0,0), rho(0,1), rho(0,2), rho(0,3)
  !read(5,*)rho(1,0), rho(1,1), rho(1,2), rho(1,3)
  !read(5,*)rho(2,0), rho(2,1), rho(2,2), rho(2,3)
  !read(5,*)rho(3,0), rho(3,1), rho(3,2), rho(3,3)
  !
  ! check input variables
  !
  if ( trim(calculation).eq.'' ) then 
    write(*,*)"input: type of calculation not specified"
    stop
  endif
  !
  if ( trim(cplexdir).eq.'' ) then 
    write(*,*)"input: cplexdir not specified"
    stop
  endif
  !
  if ( trim(tempdir).eq.'' ) then 
    write(*,*)"input: tempdir not specified"
    stop
  endif
  !
  if ( lasym ) then
    !
    if ( .not.( (nui.eq.114)  .or. (nui.eq.182)  .or. &
             (nui.eq.266)  .or. (nui.eq.366)  .or. &
             (nui.eq.482)  .or. (nui.eq.614)  .or. &
             (nui.eq.762)  .or. (nui.eq.926)  .or. & 
             (nui.eq.1106) .or. (nui.eq.1302) .or. &
             (nui.eq.1514) ) ) then
      write(*,*)"nui = ", nui
      write(*,*)"input: state with axial symmetry"
      write(*,*)"       nui must be one of the values"
      write(*,*)"       114, 182, 266, 366, 482, 614," 
      write(*,*)"       762, 926, 1106, 1302, 1514"      
      stop
    endif
    !
  else
    !
    if ( .not.((nui.eq.92) .or. (nui.eq.162) .or. (nui.eq.252)) ) then
      write(*,*)"input: nui must be in {92, 162, 252} for generic states "
      stop
    endif
    !
  endif
  !
  !
end subroutine readin
