&dirs !Don't change this symbol (required by FORTRAN)

!Using "!" for comment:

!Path to the folder that contains the excutable CPLEX:
!cplexdir = '/home/cn611340/Programs/CPLEX/CPLEX_Studio128/cplex/bin/x86-64_linux/'
cplexdir = ' /home/users/nguyen/.opt/ibm/ILOG/CPLEX_Studio128/cplex/bin/x86-64_linux/'

!Path to a folder that stores temporal files:
!This is useful when one run the program on cluster, certain location can be access fast
!For a local run, one can leave it as "./tmp/"
!tempdir = '/local/cn611340/'
tempdir= './tmp/'

/ !Don't change this symbol (required by FORTRAN)
