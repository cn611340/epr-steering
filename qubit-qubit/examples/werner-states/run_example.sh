#!/bin/sh
#
# Go to the directory where the script resides in
#
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR
#
../../bin/critical_radius.x -p path.txt -i input.txt -o output.txt
